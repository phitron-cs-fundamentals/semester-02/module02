#include<bits/stdc++.h>
using namespace std;
int main()
{
    vector<int>v={3, 5, 1, 5, 6, 10};
    // cout<<v[v.size()-1]; //access operator in back side
    // cout<<v.back();

    // cout<<v[0]; //access operator in front side
    // cout<<v.front();

    // vector <int> :: iterator it; v   //iterate operator
    for(auto it=v.begin(); it<v.end(); it++ ) // iterate instead of auto
    {
        cout<< *it << " ";
    }

    return 0;
}