#include<bits/stdc++.h>
using namespace std;
int main()
{
    //replace 
    // vector<int>v={3, 5, 1, 5, 6, 7, 8,9, 9, 9, 8, 5};
    // replace(v.begin(),v.end(), 5, 100);
    // for(int x:v)  //range based for loop
    // {
    //     cout<< x << " ";  //only used for value not for index
    // }

    //find
    vector<int>v={3, 5, 1, 5, 6, 7, 8,9, 9, 9, 8, 5};
    // vector<int>::iterator it;
    auto it=find(v.begin(),v.end(),10);
    // cout<<*it;
    if(it==v.end())cout<<"Not Found";
    else cout<<"Found";
    return 0;
}